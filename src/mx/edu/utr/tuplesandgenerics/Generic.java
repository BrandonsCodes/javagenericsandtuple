/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.edu.utr.tuplesandgenerics;

import java.util.ArrayList;
import java.util.Iterator;

public class Generic<T> implements Iterable<T> {

    private ArrayList<T> list = new ArrayList<T>();
    private int top;

    public Generic(int top) {
        super();
        this.top = top;
    }

    public void add(T object) {
        if (list.size() <= top) {

            list.add(object);
        } else {

            throw new RuntimeException("No space");
        }

    }

    public Iterator<T> iterator() {
        return list.iterator();
    }

}
